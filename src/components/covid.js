import React from 'react'
import Covid19 from '../../src/images/covid.png'

import './covid.css'

const Covid = () => {
    return(
        <div className='covid'>
            <div><a href='https://www.mohs.gov.mm/Main/content/publication/2019-ncov'><img src={Covid19} className='covid-img'></img></a></div>
        </div>
    )
}

export default Covid
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { MenuOutlined, CloseOutlined } from '@ant-design/icons'
import './dropdown.css';

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
    
      <nav id= 'nav' className='navbar'>
        <div className='navbar-container'>
          <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
           TaZe
          </Link>
          <div className='menu-icon' onClick={handleClick}>
              {click ? <CloseOutlined /> : <MenuOutlined /> }
                  
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <a href='#home' className='nav-links' onClick={closeMobileMenu}>
                Home
              </a>
            </li>
            <li className='nav-item'>
              <a
                href='#about'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                About
              </a>
            </li>
            <li className='nav-item'>
              <a
                href='#skills'
                className='nav-links'
                onClick={closeMobileMenu}
              >
               Skills
              </a>
            </li>

            <li className='nav-item'>
              <a
                href='#projects'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Projects
              </a>
            </li>
            <li className='nav-item'>
              <a
                href='#contact'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Contact
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar;

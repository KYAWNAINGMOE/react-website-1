import L_HTML5 from "../../images/html-5.svg";
import L_CSS3 from "../../images/css3.svg";
import L_REACT from "../../images/react.svg";
import L_NODE_JS from "../../images/nodejs.svg";
import L_EXPRESS from "../../images/express.svg";
import L_POSTGRESQL from "../../images/postgresql.svg";
import L_MSSQL from "../../images/mssql.svg";
import L_GIT from "../../images/git-icon.svg";
import L_HEROKU from "../../images/heroku.svg";
import L_JAVASCRIPT from "../../images/javascript.svg";
import L_TYPESCRIPT from "../../images/typescript.svg";
import L_PYTHON from "../../images/python.svg";
import Koa from '../../images/koa.jpg'
import Netlify from '../../images/netlify.png'
import Antd from '../../images/antd.png'
import Knex from '../../images/knex.png'

export const skills = {
  frontend: [
    {
      link: "https://en.wikipedia.org/wiki/HTML5",
      imgAltText: "HTML 5",
      imgSrc: L_HTML5,
      skillName: "HTML5",
    },
    {
      link: "https://medium.com/beginners-guide-to-mobile-web-development/whats-new-in-css-3-dcd7fa6122e1",
      imgAltText: "CSS 3",
      imgSrc: L_CSS3,
      skillName: "CSS3",
    },
    {
      link: "https://www.javascript.com/",
      imgAltText: "JavaScript",
      imgSrc: L_JAVASCRIPT,
      skillName: "JavaScript",
    },
    
    {
      link: "https://3x.ant.design/",
      imgAltText: "Antd Design",
      imgSrc: Antd,
      skillName: "Antd Design",
    },
    
    {
      link: "https://reactjs.org/",
      imgAltText: "React JS",
      imgSrc: L_REACT,
      skillName: "React JS",
    },
    
  ],
  backend: [
    {
      link: "https://nodejs.org/en/",
      imgAltText: "Node.js",
      imgSrc: L_NODE_JS,
      skillName: "Node.js",
    },
    {
      link: "https://expressjs.com/",
      imgAltText: "Express",
      imgSrc: L_EXPRESS,
      skillName: "Express",
    },
    {
      link: "https://knexjs.org/#changelog",
      imgAltText: "Knex.js",
      imgSrc: Knex,
      skillName: "Knex.js",
    },
    {
      link: "https://expressjs.com/",
      imgAltText: "Koa",
      imgSrc: Koa,
      skillName: "Koa",
    },
    
  ],
  hostingPlatforms: [
    {
      link: "https://www.heroku.com/",
      imgAltText: "Heroku",
      imgSrc: L_HEROKU,
      skillName: "Heroku",
    },
    
    {
      link: "https://app.netlify.com/",
      imgAltText: "Netlify",
      imgSrc: Netlify,
      skillName: "Netlify",
    },
  ],
  programmingLanguages: [
    {
      link: "https://www.javascript.com/",
      imgAltText: "JavaScript",
      imgSrc: L_JAVASCRIPT,
      skillName: "JavaScript",
    },
    {
      link: "https://www.typescriptlang.org/",
      imgAltText: "TypeScript",
      imgSrc: L_TYPESCRIPT,
      skillName: "TypeScript",
    },
    {
      link: "https://www.python.org/",
      imgAltText: "Python",
      imgSrc: L_PYTHON,
      skillName: "Python",
    },
    
  ],
  databases: [
    {
      link: "https://www.postgresql.org/",
      imgAltText: "PostgreSQL",
      imgSrc: L_POSTGRESQL,
      skillName: "PostgreSQL",
    },
    {
      link: "https://www.microsoft.com/en-us/sql-server/sql-server-2019",
      imgAltText: "MS-SQL",
      imgSrc: L_MSSQL,
      skillName: "MS-SQL",
    },
  ],
  versionControl: [
    {
      link: "https://git-scm.com/",
      imgAltText: "GIT",
      imgSrc: L_GIT,
      skillName: "GIT",
    },
  ],
};

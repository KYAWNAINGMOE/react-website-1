import React from "react";

import { Button, Row, Col, Divider } from 'antd'
import { FacebookFilled, PhoneFilled, InstagramFilled, TwitterSquareFilled, MailFilled, GithubFilled, LinkedinFilled, GitlabFilled } from '@ant-design/icons'

import './contact.css'
const ButtonGroup = Button.Group;
const Contact = () => {
  return (
    <div id="contact">
      <h1 className="contact-title">CONTACT ME</h1>

      <div className='contact-container'>
        <div className='column-left'>
          <h3><blockquote><q>Website ရှိမှ Professional Business ဖြစ်မှာပါ</q></blockquote></h3>

         <p> ဟုတ်ပါတယ်။ အခုဆိုရင် မိမိရဲ့ Products တွေကို online ကနေရောင်းလို့ ရမဲ့ ဒါမှမဟုတ် ပြထားလို့ရမဲ့ Website / Mobile App တွေဟာ ပိုပြီး အားကောင်းနေပါပြီ။ ကိုယ်ပိုင် Website / Application ရှိထားတယ်ဆိုရင် လက်ရှိမှာလည်း online ကနေ ရောင်းနေလို့ရသလလို Business တွေ ပြန်ပြီးအရှိန်အဟုန် နဲ့ လည်ပတ်လာတဲ့အခါ ကိုယ်က well plan လုပ်ပြီးသားဖြစ်နေတဲ့အတွက် အဆင်သင့်ဖြစ်နေရုံမကပဲ ကိုယ့် ပြိုင်ဖက်တွေထက် ခြေတလှမ်းသာပြီးသားဖြစ်နေမှာပါ။ ဒါ့ကြောင့် မိမိတို့ရဲ့ လုပ်ငန်း အမျိုးမျိုးအတွက် ကိုယ်ပိုင် Websiteတစ်ခုတည်ဆောက်ပြီး E-commerce လောကထဲကို ကျွန်တော့ရဲ့ ဝန်ဆောင်မှုနဲ့အတူပါဝင်လိုက်ပါ..🤣။</p>
        <p>📣 အသေးစိတ်ဆွေးနွေးလိုပါက Social Media မှဖြစ်စေ Email မှဖြစ်စေ Phone Number မှတစ်ဆင့်ဖြစ်စေ အသေးစိတ်ဆွေးနွေးနိုင်ပါတယ်</p>
        </div>

        <div className='btn-container'>
          <Row gutter={[20, 20]} justify='space-around' >
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="tel:+959-400-234-455" target="_blank" rel="noopener noreferrer">
                  <Button style={{ background: '#00cc00',  color: '#ffff', width: '140px' }} title="09 400 2344 55">
                    <PhoneFilled /> Call Me
                </Button>
                </a>
              </div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="mailto:knmcumt321@gmail.com" target="_blank" rel="noopener noreferrer">
                  <Button style={{ background: '#BB001B',  color: '#ffff', width: '140px' }} title="knmcumt321@gmail.com">
                    <MailFilled /> Email Me
                </Button>
                </a>
              </div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="mailto:kyawnaingmoe@ucsmonywa.edu.mm" target="_blank" rel="noopener noreferrer">
                  <Button style={{ background: '#0072c6',  color: '#ffff', width: '140px' }} title="kyawnaingmoe@ucsmonywa.edu.mm">
                    <MailFilled /> Outlook
                </Button>
                </a>
              </div>
            </Col>
            
            <Col className='col-contact'>
              <div className='btn-contact'><a href="https://web.facebook.com/profile.php?id=100012079187625" target="_blank" rel="noopener noreferrer">
                <Button style={{ background: '#3b5998',  color: '#ffff', width: '140px' }} title="Say hello on FB">
                  <FacebookFilled /> FaceBook
                </Button>
              </a></div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'><a href="https://www.instagram.com/m_kyaw_naing_moe/" target="_blank" rel="noopener noreferrer">
                <Button className='instagram' title="Tweets are welcomed!">
                  <InstagramFilled /> Instagram
                </Button>
              </a></div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'><a href="https://twitter.com/M_KyawNaingMoe?s=04&fbclid=IwAR2whtL5GKPDn_S4vlFzernTVeAwTJn0wropVcfeLznKp7Q7FCF8pKqneVg" target="_blank" rel="noopener noreferrer">
                <Button style={{ background: '#00acee', color: '#ffff', width: '140px' }} title="Tweets are welcomed!">
                  <TwitterSquareFilled /> Twitter
                </Button>
              </a>
              </div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="https://www.linkedin.com/in/kyawnaing-moe-078840203/" target="_blank" rel="noopener noreferrer">
                  <Button style={{ background: '#0e76a8',  color: '#ffff', width: '140px' }} title="Visit my LinkenIn">
                    <LinkedinFilled /> LinkedIn
                </Button>
                </a>
              </div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="https://github.com/kyawnaingmoe" target="_blank" rel="noopener noreferrer">
                  <Button style={{ background: '#171515', color: '#ffff', width: '140px' }} title="My other projects">
                    <GithubFilled /> GitHub
                </Button>
                </a>
              </div>
            </Col>
            <Col className='col-contact'>
              <div className='btn-contact'>
                <a href="https://gitlab.com/KYAWNAINGMOE" target="_blank" rel="noopener noreferrer">
                  <Button className='gitlab' title="My other projects">
                    <GitlabFilled /> GitLab
                </Button>
                </a>
              </div>
            </Col>


          </Row>
        </div>
      </div>
      <div className='contact-footer'>@2021 Kyaw Naing Moe</div>
    </div>
  );
};

export default Contact;
